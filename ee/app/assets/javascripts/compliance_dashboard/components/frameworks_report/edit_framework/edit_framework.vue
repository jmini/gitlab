<script>
import { GlAlert, GlButton, GlForm, GlLoadingIcon, GlTooltip } from '@gitlab/ui';
import * as Sentry from '~/sentry/sentry_browser_wrapper';
import { sprintf } from '~/locale';
import { SAVE_ERROR } from 'ee/groups/settings/compliance_frameworks/constants';
import {
  getSubmissionParams,
  initialiseFormData,
} from 'ee/groups/settings/compliance_frameworks/utils';
import { fromYaml } from 'ee/security_orchestration/components/policy_editor/pipeline_execution/utils';
import { getIdFromGraphQLId } from '~/graphql_shared/utils';
import { ROUTE_NEW_FRAMEWORK_SUCCESS } from '../../../constants';
import { convertFrameworkIdToGraphQl } from '../../../utils';
import createComplianceFrameworkMutation from '../../../graphql/mutations/create_compliance_framework.mutation.graphql';
import updateComplianceFrameworkMutation from '../../../graphql/mutations/update_compliance_framework.mutation.graphql';
import deleteComplianceFrameworkMutation from '../../../graphql/mutations/delete_compliance_framework.mutation.graphql';
import createComplianceRequirement from '../../../graphql/mutations/create_compliance_requirement.mutation.graphql';
import getComplianceFrameworkQuery from './graphql/get_compliance_framework.query.graphql';
import DeleteModal from './components/delete_modal.vue';
import BasicInformationSection from './components/basic_information_section.vue';
import RequirementsSection from './components/requirements_section.vue';
import PoliciesSection from './components/policies_section.vue';
import ProjectsSection from './components/projects_section.vue';
import { i18n } from './constants';

export default {
  components: {
    BasicInformationSection,
    PoliciesSection,
    ProjectsSection,
    RequirementsSection,
    DeleteModal,
    GlAlert,
    GlButton,
    GlForm,
    GlLoadingIcon,
    GlTooltip,
  },
  inject: [
    'pipelineConfigurationFullPathEnabled',
    'groupPath',
    'featureSecurityPoliciesEnabled',
    'adherenceV2Enabled',
  ],
  data() {
    return {
      errorMessage: '',
      formData: initialiseFormData(),
      requirements: [],
      originalName: '',
      isBasicInformationValid: true,
      isSaving: false,
      isDeleting: false,
      hasMigratedPipeline: false,
    };
  },
  apollo: {
    // eslint-disable-next-line @gitlab/vue-no-undef-apollo-properties
    namespace: {
      query: getComplianceFrameworkQuery,
      variables() {
        return {
          fullPath: this.groupPath,
          complianceFramework: this.graphqlId,
        };
      },
      result({ data }) {
        const [complianceFramework] = data?.namespace?.complianceFrameworks?.nodes || [];
        if (complianceFramework) {
          const { mockRequirements, ...rest } = complianceFramework;
          this.formData = { ...rest };
          this.requirements = mockRequirements?.nodes ? [...mockRequirements.nodes] : [];
          this.originalName = complianceFramework.name;
          const policyBlob =
            data.namespace.securityPolicyProject?.repository?.blobs?.nodes?.[0]?.rawBlob;
          if (policyBlob) {
            const id = getIdFromGraphQLId(this.graphqlId);
            const contents = fromYaml({ manifest: policyBlob });
            this.hasMigratedPipeline = Boolean(
              contents?.pipeline_execution_policy?.find((policy) => {
                return (
                  policy?.policy_scope?.compliance_frameworks?.find((f) => f.id === id) &&
                  policy?.metadata?.compliance_pipeline_migration
                );
              }),
            );
          }
        } else {
          this.errorMessage = this.$options.i18n.fetchError;
        }
      },
      error(error) {
        this.errorMessage = this.$options.i18n.fetchError;
        Sentry.captureException(error);
      },
      skip() {
        return this.isNewFramework;
      },
    },
  },
  computed: {
    isLoading() {
      return this.$apollo.loading || this.isSaving;
    },
    isNewFramework() {
      return !this.$route.params.id;
    },
    isDefaultFramework() {
      return this.formData.default;
    },
    hasLinkedPolicies() {
      return Boolean(
        this.formData.scanResultPolicies?.pageInfo.startCursor ||
          this.formData.scanExecutionPolicies?.pageInfo.startCursor ||
          this.formData.pipelineExecutionPolicies?.pageInfo.startCursor,
      );
    },
    deleteBtnDisabled() {
      return this.hasLinkedPolicies || this.isDefaultFramework;
    },
    deleteBtnDisabledTooltip() {
      return this.isDefaultFramework
        ? i18n.deleteButtonDefaultFrameworkDisabledTooltip
        : i18n.deleteButtonLinkedPoliciesDisabledTooltip;
    },
    refetchConfig() {
      return {
        awaitRefetchQueries: true,
        refetchQueries: [
          {
            query: getComplianceFrameworkQuery,
            variables: {
              fullPath: this.groupPath,
            },
          },
        ],
      };
    },
    title() {
      return this.isNewFramework
        ? this.$options.i18n.addFrameworkTitle
        : sprintf(this.$options.i18n.editFrameworkTitle, { frameworkName: this.originalName });
    },
    saveButtonText() {
      return this.isNewFramework
        ? this.$options.i18n.addSaveBtnText
        : this.$options.i18n.editSaveBtnText;
    },
    graphqlId() {
      return this.$route.params.id ? convertFrameworkIdToGraphQl(this.$route.params.id) : null;
    },
    disableSubmitBtn() {
      return !this.isBasicInformationValid;
    },
    shouldRenderPolicySection() {
      return !this.isNewFramework && this.featureSecurityPoliciesEnabled;
    },
  },
  methods: {
    setError(error, userFriendlyText, loadingProp = 'isSaving') {
      this[loadingProp] = false;
      this.errorMessage = userFriendlyText;
      Sentry.captureException(error);
    },
    navigateBack() {
      this.$router.back();
    },
    async createFramework(params) {
      const { data } = await this.$apollo.mutate({
        mutation: createComplianceFrameworkMutation,
        variables: {
          input: {
            namespacePath: this.groupPath,
            params,
          },
        },
      });

      const framework = data?.createComplianceFramework?.framework;
      const errors = data?.createComplianceFramework?.errors;

      if (errors && errors.length) {
        throw new Error(errors[0]);
      }

      return framework.id;
    },
    async updateFramework(params) {
      const { data } = await this.$apollo.mutate({
        mutation: updateComplianceFrameworkMutation,
        variables: {
          input: {
            id: this.graphqlId,
            params,
          },
        },
      });

      const framework = data?.updateComplianceFramework?.complianceFramework;
      const errors = data?.updateComplianceFramework?.errors;

      if (errors && errors.length) {
        throw new Error(errors[0]);
      }

      return framework.id;
    },
    async onSubmit() {
      this.isSaving = true;
      this.errorMessage = '';
      try {
        const params = getSubmissionParams(
          this.formData,
          this.pipelineConfigurationFullPathEnabled,
        );

        let frameworkId;

        if (this.isNewFramework) {
          frameworkId = await this.createFramework(params);
          if (this.adherenceV2Enabled) {
            await this.createRequirements(frameworkId);
          }
        } else {
          frameworkId = await this.updateFramework(params);
        }

        this.handleMutationSuccess(this.$route.params.id || frameworkId);
      } catch (error) {
        this.setError(error, SAVE_ERROR);
      } finally {
        this.isSaving = false;
      }
    },
    handleMutationSuccess(id) {
      if (this.isNewFramework) {
        this.$router.push({
          name: ROUTE_NEW_FRAMEWORK_SUCCESS,
          params: { id },
        });
      } else {
        this.navigateBack();
      }
    },
    async createRequirements(frameworkId) {
      const newRequirements = this.requirements.filter((requirement) => !requirement.id);

      if (newRequirements.length === 0) {
        return;
      }

      const createRequirementPromises = newRequirements.map((requirement) =>
        this.createRequirement(requirement, frameworkId),
      );

      await Promise.all(createRequirementPromises);
    },
    async createRequirement(requirement, frameworkId) {
      const { data } = await this.$apollo.mutate({
        mutation: createComplianceRequirement,
        variables: {
          input: {
            complianceFrameworkId: frameworkId,
            params: {
              name: requirement.name,
              description: requirement.description,
            },
          },
        },
      });

      const errors = data?.createComplianceRequirement?.errors;

      if (errors && errors.length) {
        throw new Error(errors[0]);
      }
    },
    async updateRequirements(requirement) {
      if (this.isNewFramework) {
        this.requirements.push(requirement);
      } else {
        try {
          await this.createRequirement(requirement, this.graphqlId);
        } catch (error) {
          this.setError(error, error);
        }
      }
    },
    async deleteFramework() {
      this.isDeleting = true;

      try {
        const {
          data: { destroyComplianceFramework },
        } = await this.$apollo.mutate({
          mutation: deleteComplianceFrameworkMutation,
          variables: {
            input: {
              id: this.graphqlId,
            },
          },
          ...this.refetchConfig,
        });

        const [error] = destroyComplianceFramework.errors;

        if (error) {
          throw error;
        }
        this.$router.back();
      } catch (error) {
        this.setError(new Error(error), error, 'isDeleting');
      }
    },
    onDelete() {
      this.$refs.deleteModal.show();
    },
  },
  i18n,
};
</script>

<template>
  <div class="gl-mt-7">
    <gl-alert v-if="errorMessage" class="gl-mb-7" variant="danger" :dismissible="false">
      {{ errorMessage }}
    </gl-alert>
    <gl-loading-icon v-if="isLoading" size="lg" />

    <template v-else>
      <h2 class="gl-heading-2 gl-mb-7">{{ title }}</h2>
      <gl-form @submit.prevent="onSubmit">
        <basic-information-section
          v-if="formData"
          v-model="formData"
          :is-expanded="isNewFramework"
          :has-migrated-pipeline="hasMigratedPipeline"
          @valid="isBasicInformationValid = $event"
        />

        <requirements-section
          v-if="adherenceV2Enabled"
          :requirements="requirements"
          :is-new-framework="isNewFramework"
          @save="updateRequirements"
        />

        <policies-section
          v-if="shouldRenderPolicySection"
          :full-path="groupPath"
          :graphql-id="graphqlId"
        />

        <projects-section v-if="!isNewFramework" :compliance-framework="formData" />

        <div class="gl-flex gl-gap-3 gl-px-5 gl-pt-6">
          <gl-button
            type="submit"
            variant="confirm"
            class="js-no-auto-disable"
            data-testid="submit-btn"
            :disabled="disableSubmitBtn"
          >
            {{ saveButtonText }}
          </gl-button>
          <gl-button data-testid="cancel-btn" @click="navigateBack">{{ __('Cancel') }}</gl-button>
          <template v-if="graphqlId">
            <gl-tooltip
              v-if="deleteBtnDisabled"
              :target="() => $refs.deleteBtn"
              :title="deleteBtnDisabledTooltip"
            />
            <div ref="deleteBtn" class="gl-ml-auto">
              <gl-button
                variant="danger"
                data-testid="delete-btn"
                :loading="isDeleting"
                :disabled="deleteBtnDisabled"
                @click="onDelete"
              >
                {{ $options.i18n.deleteButtonText }}
              </gl-button>
            </div>
          </template>
        </div>
      </gl-form>
    </template>

    <delete-modal
      v-if="graphqlId"
      ref="deleteModal"
      :name="originalName"
      @delete="deleteFramework"
    />
  </div>
</template>
